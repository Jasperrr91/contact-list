import {
  cleanPhoneString,
  formatPhoneString,
} from './phone';

describe('utils phone', () => {
  describe('function cleanPhoneString', () => {
    it('should remove all non-numbers', () => {
      expect(cleanPhoneString('06-12345678')).toEqual('0612345678');
      expect(cleanPhoneString('(035) 123 4567')).toEqual('0351234567');
    });

    it('should limit phone number to 10 numbers', () => {
      expect(cleanPhoneString('06-123456789')).toEqual('0612345678');
    });

    it('should default to 0612345678 if no/incorrect number given', () => {
      expect(cleanPhoneString('abc')).toEqual('0612345678');
      expect(cleanPhoneString('')).toEqual('0612345678');
    });
  });

  describe('function formatPhoneString', () => {
    it('should format a phone string into a prettier format', () => {
      expect(formatPhoneString('0612345678')).toEqual('061 234 56 78');
      expect(formatPhoneString('0351234567')).toEqual('035 123 45 67');
    });
  });
});

export const cleanPhoneString = (phone: string): string => {
  const matches = phone.match(/([0-9])/gm);
  if (matches) return matches.slice(0, 10).join('');
  return '0612345678'; // fallback number
};

export const formatPhoneString = (phone: string): string => {
  const part1 = phone.substring(0, 3);
  const part2 = phone.substring(3, 6);
  const part3 = phone.substring(6, 8);
  const part4 = phone.substring(8, 10);
  return `${part1} ${part2} ${part3} ${part4}`;
};

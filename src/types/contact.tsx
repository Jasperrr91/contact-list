export type Contact = {
  id: number,
  name: string,
  phone: string,
  email: string,
  status: Status,
};

export type Status = 'private' | 'work';

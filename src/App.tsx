import React from 'react';
import { Provider } from 'react-redux';

import ContactList from './components/ContactList/ContactList';
import store from './store/store';

import './App.css';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <ContactList />
      </div>
    </Provider>
  );
}

export default App;

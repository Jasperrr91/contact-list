import {
  randomUserToContact,
} from './contactApi';

import type { RandomApiResult } from './contactApi';

const RANDOM_API_RESULT: RandomApiResult = {
  results: [{
    name: {
      first: 'Jasper',
      last: 'van der Stoop',
    },
    phone: '(06) 12 34 56 78',
    email: 'jasper@vanderstoop.nl',
  }],
};

describe('api contact', () => {
  describe('function randomUserToContact', () => {
    it('should remove all non-numbers', async () => {
      const mockMath = Object.create(global.Math);
      mockMath.random = () => 0.5;
      global.Math = mockMath;

      const expectedContact = {
        name: 'Jasper van der Stoop',
        phone: '0612345678',
        email: 'jasper@vanderstoop.nl',
        status: 'private',
        id: 0,
      };
      expect(await randomUserToContact(RANDOM_API_RESULT)).toEqual(expectedContact);
    });
  });
});

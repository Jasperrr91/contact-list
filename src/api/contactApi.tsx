import type { Contact } from '../types';
import { cleanPhoneString } from '../utils';

export type RandomApiResult = {
  results: {
    name: {
      first: string,
      last: string,
    },
    phone: string,
    email: string,
  }[],
};

export const randomUserToContact = async (data: RandomApiResult) => {
  const { name, phone, email } = data.results[0];
  const { first, last } = name;
  const status = (Math.random() < 0.5) ? 'work' : 'private';

  const newContact: Contact = {
    name: `${first} ${last}`,
    phone: cleanPhoneString(phone),
    email,
    status,
    id: 0,
  };
  return newContact;
};

export const getRandomContact = async () => {
  const res = await fetch('https://randomuser.me/api/');
  return randomUserToContact(await res.json());
};

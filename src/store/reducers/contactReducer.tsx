import type { Contact } from '../../types';

const nextId = (contacts: Contact[]) => {
  if (contacts.length === 0) return 1;
  const currentMaxId = contacts.reduce(
    (maxId: number, contact: any) => Math.max(contact.id, maxId), -1,
  );
  return currentMaxId + 1;
};

type ContactAction = {
  type: 'ADD_CONTACT' | 'DELETE_CONTACT' | 'UPDATE_CONTACT',
  payload: any,
};

const contactReducer = (state: Contact[] = [], action: ContactAction) => {
  switch (action.type) {
    case 'ADD_CONTACT':
      return [
        ...state,
        {
          ...action.payload,
          id: nextId(state),
        },
      ];
    case 'DELETE_CONTACT':
      return [
        ...state,
      ].filter((contact) => {
        if (contact.id === action.payload) return false;
        return true;
      });
    case 'UPDATE_CONTACT':
      return [
        ...state,
      ].map((contact) => {
        if (contact.id === action.payload.id) {
          return action.payload;
        }
        return contact;
      });
    default:
      return state;
  }
};

export type ContactState = ReturnType<typeof contactReducer>;

export default contactReducer;

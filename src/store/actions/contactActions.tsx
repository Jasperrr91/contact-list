import ContactActionTypes from './actionTypes';

import type { Contact } from '../../types';

const { ADD_CONTACT, DELETE_CONTACT, UPDATE_CONTACT } = ContactActionTypes;

export function addContact(contact: Contact) {
  return { type: ADD_CONTACT, payload: contact };
}

export function deleteContact(id: number) {
  return { type: DELETE_CONTACT, payload: id };
}

export function updateContact(contact: Contact) {
  return { type: UPDATE_CONTACT, payload: contact };
}

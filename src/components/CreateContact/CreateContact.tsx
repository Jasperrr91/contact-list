import React from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Form,
  Modal,
  Input,
  Select,
} from 'antd';

import { addContact } from '../../store/actions/contactActions';

import type { Contact } from '../../types';

const { Option } = Select;

type Props = {
  toggle: () => void,
  dispatchContact: (arg: Contact) => Contact,
};

const CreateContact = ({ toggle, dispatchContact }: Props) => {
  const onFinish = (values: any): void => {
    dispatchContact(values);
    toggle();
  };

  const onFinishFailed = (errorInfo: any): void => {
    console.error('Failed:', errorInfo);
  };

  return (
    <Modal
      visible
      title="Add contact"
      onCancel={toggle}
      footer={[
        <Button key="back" onClick={toggle}>
          Return
            </Button>,
        <Button form="create-contact-form" htmlType="submit" key="submit" type="primary">
          Submit
            </Button>,
      ]}
    >
      <Form
        id="create-contact-form"
        layout="horizontal"
        name="new-contact"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[
            {
              required: true,
              message: 'Please input a name',
            },
          ]}
        >
          <Input placeholder="John Doe" />
        </Form.Item>
        <Form.Item
          label="Phone"
          name="phone"
          rules={[
            {
              required: true,
              message: 'Please input a phonenumber',
            },
          ]}
        >
          <Input placeholder="0612345678" />
        </Form.Item>
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: 'Please input a email address',
            },
          ]}
        >
          <Input placeholder="steve@apple.com" />
        </Form.Item>
        <Form.Item
          label="Status"
          name="status"
          rules={[
            {
              required: true,
              message: 'Please select a status',
            },
          ]}
        >
          <Select placeholder='Choose status' style={{ width: 120 }}>
            <Option value="work">Work</Option>
            <Option value="private">Private</Option>
          </Select>
        </Form.Item>
      </Form>
    </Modal>
  );
};

const mapDispatchToProps = (dispatch: any) => ({
  dispatchContact: (contact: Contact) => dispatch(addContact(contact)),
});

export default connect(null, mapDispatchToProps)(CreateContact);

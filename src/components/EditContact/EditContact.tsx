import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Form,
  Input,
  Modal,
  Select,
} from 'antd';

import { updateContact } from '../../store/actions/contactActions';

import type { Contact } from '../../types';

type Props = {
  contact: Contact,
  toggle: () => void,
  dispatchContact: (arg: Contact) => Contact,
};

type Fields = {
  name: string[],
  value: string | number,
}[];

const { Option } = Select;

const mapContactToFields = (contact: Contact): Fields => ([
  {
    name: ['name'],
    value: contact.name,
  },
  {
    name: ['phone'],
    value: contact.phone,
  },
  {
    name: ['email'],
    value: contact.email,
  },
  {
    name: ['status'],
    value: contact.status,
  },
  {
    name: ['id'],
    value: contact.id,
  },
]);

const EditContact = ({ contact, toggle, dispatchContact }: Props) => {
  const [fields, setFields] = useState<Fields>();

  useEffect(() => {
    setFields(mapContactToFields(contact));
  }, [contact]);

  const onFinish = (values: any) => {
    dispatchContact(values);
    toggle();
  };

  const onFinishFailed = (errorInfo: any) => {
    console.error('Failed:', errorInfo);
  };

  return (
    <Modal
      visible
      title="Edit contact"
      onCancel={toggle}
      footer={[
        <Button key="back" onClick={toggle}>
          Return
        </Button>,
        <Button form="create-contact-form" htmlType="submit" key="submit" type="primary">
          Submit
        </Button>,
      ]}
    >
      <Form
        id="create-contact-form"
        layout="horizontal"
        name="new-contact"
        fields={fields}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[
            {
              required: true,
              message: 'Please input a name',
            },
          ]}
        >
          <Input placeholder="John Doe" />
        </Form.Item>
        <Form.Item
          label="Phone"
          name="phone"
          rules={[
            {
              required: true,
              message: 'Please input a phonenumber',
            },
          ]}
        >
          <Input placeholder="0612345678" />
        </Form.Item>
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: 'Please input a email address',
            },
          ]}
        >
          <Input placeholder="steve@apple.com" />
        </Form.Item>
        <Form.Item
          label="Status"
          name="status"
          rules={[
            {
              required: true,
              message: 'Please select a status',
            },
          ]}
        >
          <Select placeholder='Choose status' style={{ width: 120 }}>
            <Option value="work">Work</Option>
            <Option value="private">Private</Option>
          </Select>
        </Form.Item>
        <Form.Item hidden name="id" />
      </Form>
    </Modal>
  );
};

const mapDispatchToProps = (dispatch: any) => ({
  dispatchContact: (contact: Contact) => dispatch(updateContact(contact)),
});

export default connect(null, mapDispatchToProps)(EditContact);

import React from 'react';
import { connect } from 'react-redux';
import { Card, Avatar } from 'antd';
import {
  DeleteOutlined,
  EditOutlined,
  HomeFilled,
  LinkedinFilled,
  MailOutlined,
  PhoneOutlined,
} from '@ant-design/icons';

import { deleteContact } from '../../store/actions/contactActions';
import { formatPhoneString } from '../../utils';

import './ContactCard.css';

import type { Contact, Status } from '../../types';

type Props = {
  contact: Contact,
  dispatchDeleteContact: (arg: number) => number,
  editToggle: (arg: Contact) => void,
};

const { Meta } = Card;

export function ContactCard({ contact, dispatchDeleteContact, editToggle }: Props) {
  const {
    name,
    phone,
    email,
    status,
    id,
  } = contact;

  const renderStatus = (contactStatus: Status) => {
    const icon = contactStatus === 'work' ? <LinkedinFilled /> : <HomeFilled />;
    return <Avatar icon={icon} shape="circle" />;
  };

  return (
    <div>
      <Card style={{ width: 300, marginTop: 16 }} className="contact-card" actions={[
        <DeleteOutlined key="delete" onClick={() => dispatchDeleteContact(id)} />,
        <EditOutlined key="edit" onClick={() => editToggle(contact)} />,
      ]}>
        <Meta
          avatar={renderStatus(status)}
          title={name}
          description={
            <ul>
              <li><MailOutlined /><span className="value">{email}</span></li>
              <li><PhoneOutlined /><span className="value">{formatPhoneString(phone)}</span></li>
            </ul>
          }
        />
      </Card>
    </div>
  );
}

const mapDispatchToProps = (dispatch: any) => ({
  dispatchDeleteContact: (id: number) => dispatch(deleteContact(id)),
});

export default connect(null, mapDispatchToProps)(ContactCard);

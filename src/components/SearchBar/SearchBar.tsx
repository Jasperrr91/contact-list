import React from 'react';
import { Form, Input, Select } from 'antd';
import {
  MailOutlined,
  PhoneOutlined,
  UserOutlined,
} from '@ant-design/icons';

import './SearchBar.css';

const { Option } = Select;

type Props = {
  handleSearchName: (arg: string) => void,
  handleSearchPhone: (arg: string) => void,
  handleSearchEmail: (arg: string) => void,
  handleSearchStatus: (arg: string) => void,
};

const SearchBar = ({
  handleSearchName,
  handleSearchPhone,
  handleSearchEmail,
  handleSearchStatus,
}: Props) => (
  <Form name="search_bar" layout="inline" className="search-bar">
    <span className="search-bar__label">Search:</span>
    <Form.Item>
      <Input prefix={<UserOutlined />} onChange={(e) => handleSearchName(e.target.value)} name="name" placeholder="name" />
    </Form.Item>
    <Form.Item>
      <Input prefix={<PhoneOutlined />} onChange={(e) => handleSearchPhone(e.target.value)} name="phone" placeholder="phone" />
    </Form.Item>
    <Form.Item>
      <Input prefix={<MailOutlined />} onChange={(e) => handleSearchEmail(e.target.value)} name="email" placeholder="email" />
    </Form.Item>
    <Form.Item name="status">
      <Select onChange={(val) => handleSearchStatus(val)} defaultValue="both" placeholder='Choose status' style={{ width: 120 }}>
        <Option value="both">Both</Option>
        <Option value="work">Work</Option>
        <Option value="private">Private</Option>
      </Select>
    </Form.Item>
  </Form>
);

export default SearchBar;

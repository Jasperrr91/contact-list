import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Button, Row, Col } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

import ContactCard from '../ContactCard/ContactCard';
import CreateContact from '../CreateContact/CreateContact';
import EditContact from '../EditContact/EditContact';
import SearchBar from '../SearchBar/SearchBar';

import { getRandomContact } from '../../api/contactApi';
import { addContact } from '../../store/actions/contactActions';

import './ContactList.css';

import type { ContactState } from '../../store/reducers/contactReducer';
import type { Contact } from '../../types';

type Props = {
  contacts: Contact[],
  dispatchContact: (arg: Contact) => Contact,
};

export const containsValue = (haystack: string, needle: string) => {
  if (haystack.toLowerCase().includes(needle.toLowerCase())) return true;
  return false;
};

const ContactList = ({ contacts, dispatchContact }: Props) => {
  const [showCreate, setShowCreate] = useState<boolean>(false);
  const [showEdit, setShowEdit] = useState<boolean>(false);
  const [editContact, setEditContact] = useState<Contact>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [filteredContacts, setFilteredContacts] = useState<Contact[]>(contacts);

  const [searchName, setSearchName] = useState<string>('');
  const [searchPhone, setSearchPhone] = useState<string>('');
  const [searchEmail, setSearchEmail] = useState<string>('');
  const [searchStatus, setSearchStatus] = useState<string>('both');

  const addRandomContact = async () => {
    setIsLoading(true);
    dispatchContact(await getRandomContact());
    setIsLoading(false);
  };

  useEffect(() => {
    if (contacts.length === 0) addRandomContact();
  }, []);

  useEffect(() => {
    setFilteredContacts(contacts);
  }, [contacts]);

  useEffect(() => {
    setFilteredContacts(contacts.filter((contact) => {
      if (!containsValue(contact.name, searchName)) return false;
      if (!containsValue(contact.phone, searchPhone)) return false;
      if (!containsValue(contact.email, searchEmail)) return false;
      if (searchStatus !== 'both') return contact.status === searchStatus;
      return true;
    }));
  }, [searchName, searchPhone, searchEmail, searchStatus]);

  const toggleShowCreate = () => setShowCreate(!showCreate);
  const toggleShowEdit = () => setShowEdit(!showEdit);

  const renderCreateContact = () => {
    if (showCreate) {
      return <CreateContact toggle={toggleShowCreate} />;
    }
    return '';
  };

  const renderEditContact = () => {
    if (showEdit && editContact) {
      return <EditContact toggle={toggleShowEdit} contact={editContact} />;
    }
    return '';
  };

  const onEdit = (contact: Contact) => {
    toggleShowEdit();
    setEditContact(contact);
    setFilteredContacts(contacts);
  };

  return (
    <div>
      {renderCreateContact()}
      {renderEditContact()}

      <Row justify="center" className="action-buttons">
        <Col xs={24} md={4}>
          <Button type="primary" icon={<PlusOutlined />} onClick={toggleShowCreate}>Add Contact</Button>
        </Col>
        <Col xs={24} md={4}>
          <Button type="primary" icon={<PlusOutlined />} onClick={addRandomContact}>Generate Random Contact</Button>
        </Col>
      </Row>

      {contacts.length === 0
        ? <Row justify="center">
          {isLoading
            ? <LoadingOutlined className="loading-icon" />
            : <Col xs={24} md={12} className="no-contact-warning">
              <p>You have no contacts in your contact list.
              Please use the buttons above to either manually add a contact or generate a
        random contact.</p>
            </Col>}
        </Row>
        : <>
          <Row justify="center">
            <SearchBar
              handleSearchName={setSearchName}
              handleSearchPhone={setSearchPhone}
              handleSearchEmail={setSearchEmail}
              handleSearchStatus={setSearchStatus}
            />
          </Row>
          <Row>
            {filteredContacts.map((contact: Contact) => (
              <Col md={6} key={contact.id}>
                <ContactCard contact={contact} editToggle={onEdit} />
              </Col>
            ))}
          </Row>
        </>}
    </div>
  );
};

const mapStateToProps = (contacts: ContactState) => ({
  contacts,
});

const mapDispatchToProps = (dispatch: any) => ({
  dispatchContact: (contact: Contact) => dispatch(addContact(contact)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);
